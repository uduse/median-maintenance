#include "stdafx.h"
#include "MaxHeap.h"
#include <iostream>


MaxHeap::MaxHeap()
{
}

void MaxHeap::Heapify()
{
	int length = array.size();
	for ( int i = length - 1; i >= 0; --i )
	{
		BubbleDown( i );
	}
}

void MaxHeap::BubbleDown( int index )
{
	int length = array.size();
	int leftChildIndex = 2 * index + 1;
	int rightChildIndex = 2 * index + 2;
	//cout << "L: " << leftChildIndex << "\tR: " << rightChildIndex << endl;

	if ( leftChildIndex >= length )
		return; //index is a leaf

	int maxIndex = index;

	if ( array[index] < array[leftChildIndex] )
	{
		maxIndex = leftChildIndex;
		//cout << "Turn Left, Max Index : " << maxIndex << endl;
	}

	if ( ( rightChildIndex < length ) && ( array[maxIndex] < array[rightChildIndex] ) )
	{
		maxIndex = rightChildIndex;
	}

	if ( maxIndex != index )
	{
		//need to swap
		int temp = array[index];
		array[index] = array[maxIndex];
		array[maxIndex] = temp;
		BubbleDown( maxIndex );
	}
}

void MaxHeap::BubbleUp( int index )
{
	if ( index == 0 )
		return;

	int parentIndex = ( index - 1 ) / 2;

	if ( array[parentIndex] < array[index] )
	{
		//need to swap
		int temp = array[parentIndex];
		array[parentIndex] = array[index];
		array[index] = temp;
		BubbleUp( parentIndex );
	}
}

void MaxHeap::Insert( int newPacket )
{
	int length = array.size();

	array.push_back( newPacket );

	BubbleUp( array.size() - 1 );
}

int MaxHeap::GetMax()
{
	return array.front();
}

int MaxHeap::heapSize()
{
	return array.size();
}

void MaxHeap::DeleteMax()
{
	int length = array.size();

	if ( length == 0 )
	{
		return;
	}

	swap( array.front(), array.back() );

	array.pop_back();
	BubbleDown( 0 );

	//return true;
}

void MaxHeap::print()const
{
	cout << endl;
	for ( int pos = 0; pos < array.size(); pos++ )
	{
		cout << array[pos] << " ";
	}
	cout << endl;
}


