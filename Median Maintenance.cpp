// Median Maintenance.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MinHeap.h"
#include "MaxHeap.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <time.h>

using namespace std;

MaxHeap leftMaxHeap;
MinHeap rightMinHeap;

long long medianCount = 0;

int getMedian();
void insert( int temp );
void balance();

int getMedian()
{
	int median;
	if ( leftMaxHeap.heapSize() > rightMinHeap.heapSize() )
	{
		median = leftMaxHeap.GetMax();
	}
	else if ( rightMinHeap.heapSize() > leftMaxHeap.heapSize() )
	{
		median = rightMinHeap.GetMin();
	}
	else
	{
		median = leftMaxHeap.GetMax();
	}
	return median;
}

void insert( int value )
{
	if ( rightMinHeap.heapSize() == 0 || value < leftMaxHeap.GetMax() )
	{
		leftMaxHeap.Insert( value );
	}
	else
	{
		rightMinHeap.Insert( value );
	}
}

void balance()
{
	if ( leftMaxHeap.heapSize() > rightMinHeap.heapSize() + 1 )
	{
		rightMinHeap.Insert( leftMaxHeap.GetMax() );
		leftMaxHeap.DeleteMax();
	}
	else if ( rightMinHeap.heapSize() > leftMaxHeap.heapSize() + 1 )
	{
		leftMaxHeap.Insert( rightMinHeap.GetMin() );
		rightMinHeap.DeleteMin();
	}
}



void print()
{
	leftMaxHeap.print();
	rightMinHeap.print();
	cout << endl; cout << endl;
}

int _tmain( int argc, _TCHAR* argv[] )
{
	clock_t begin = clock();

	int temp2;
	int temp;
	fstream input( "Median.txt" );
	//fstream input( "1.txt" );
	//fstream input( "2.txt" );



	while ( input >> temp )
	{
		insert( temp );
		balance();
		medianCount += getMedian();
		//print();
		//cout << "Median: " << getMedian(); cout << endl;
		//cin.get();
	}

	cout << medianCount;


	clock_t end = clock();
	double elapsed_secs = double( end - begin ) / CLOCKS_PER_SEC;
	cout << endl;
	cout << "Running Time: " << elapsed_secs; cout << endl;

	return 0;
}

