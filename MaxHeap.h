#pragma once

#include <vector>

using namespace std;

class MaxHeap
{
private:


public:
	void BubbleDown( int index );
	void BubbleUp( int index );
	void Heapify();
	//MaxHeap( int* array, int length );
	//MaxHeap( const vector<Packet>& vector );
	
	MaxHeap();
	vector<int> array;


	void Insert( int newPacket );
	int GetMax();
	int heapSize();
	void DeleteMax();
	void print()const;
	bool isEmpty();
};

