#pragma once

#include <vector>

using namespace std;

class MinHeap
{
private:

public:
	void BubbleDown( int index );
	void BubbleUp( int index );
	void Heapify();
	//MinHeap( int* array, int length );
	//MinHeap( const vector<Packet>& vector );
	
	MinHeap();
	vector<int> array;


	void Insert( int newPacket );
	int GetMin();
	int heapSize();
	void DeleteMin();
	void print()const;
	bool isEmpty();
};